# Car-Eye-ES 智慧环卫管理平台

# 服务项目

![输入图片说明](https://gitee.com/careye_open_source_platform_group/car-eye-es/raw/master/service.png)

# 产品特色

![输入图片说明](https://gitee.com/careye_open_source_platform_group/car-eye-es/raw/master/tese.png)

# 平台架构

![输入图片说明](https://gitee.com/careye_open_source_platform_group/car-eye-es/raw/master/framwork.png)

# 功能一览

## 环卫一张图

![输入图片说明](https://gitee.com/careye_open_source_platform_group/car-eye-es/raw/master/overview.png)


## 人员保洁

![输入图片说明](https://gitee.com/careye_open_source_platform_group/car-eye-es/raw/master/renyuan.png)

## 监督考核

![输入图片说明](https://gitee.com/careye_open_source_platform_group/car-eye-es/raw/master/rule.png)
![输入图片说明](https://gitee.com/careye_open_source_platform_group/car-eye-es/raw/master/case.png)

## 垃圾收运

![输入图片说明](https://gitee.com/careye_open_source_platform_group/car-eye-es/raw/master/laji.png)


## 手机APP

![输入图片说明](https://gitee.com/careye_open_source_platform_group/car-eye-es/raw/master/APP.png)

# 平台体验
www.liveoss.com:7000 
账号：test
密码：123456
  
# 联系我们

car-eye 开源官方网址：www.car-eye.cn    
car-eye 环卫管理平台网址: www.liveoss.com:7000
car-eye 车辆管理平台网址：www.liveoss.com  
car-eye GB28181管理平台网址 ：www.liveoss.com:10088     
car-eye 技术官方邮箱: support@car-eye.cn  
car-eye 车辆管理平台技术交流QQ群: 590411159   
car-eye 视频服务和管理平台QQ群：713522732   


![](https://gitee.com/careye_open_source_platform_group/car-eye-jtt1078-media-server/raw/master/QQ/QQ.jpg)     
CopyRight©  car-eye 开源团队 2018-2020

